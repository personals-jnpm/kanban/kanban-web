import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';
import {AuthService} from "../services/auth.service";

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(private authService: AuthService) {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    console.log(request.url)
    if (request.url.includes(`${this.authService.userHostUrl}register`) ||
      request.url.includes(`${this.authService.userHostUrl}login`)) {
      return next.handle(request);
    }
    this.authService.loadToken();
    return next.handle(request.clone({setHeaders: {authorization: `Bearer ${this.authService.token}`}}));
  }
}
