import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import { KanbanComponent } from './kanban/kanban.component';
import {FormsModule} from "@angular/forms";
import {IncludesModule} from "../includes/includes.module";
import {AppModule} from "../../app.module";
import {TimeAgoPipe} from "../../pipes/TimeAgoPipe";


@NgModule({
  declarations: [
    KanbanComponent,
    TimeAgoPipe
  ],
  exports: [
    KanbanComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    IncludesModule,
  ]
})
export class BoardModule {
}
