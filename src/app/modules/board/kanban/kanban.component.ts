import {Component, OnInit} from '@angular/core';
import {BoardService} from "../../../services/board.service";
import {TaskKanban} from "../../../dtos/response/TaskKanban";
import {StateTask} from "../../../dtos/response/StateTask";
import {Board} from "../../../dtos/response/Board";
import {NotificationService} from "../../../services/notification.service";
import {TaskRequest} from "../../../dtos/request/board/TaskRequest";

@Component({
  selector: 'app-kanban',
  templateUrl: './kanban.component.html',
  styleUrls: ['./kanban.component.css']
})
export class KanbanComponent implements OnInit {

  public currentTask: TaskKanban;
  public currentState: StateTask;

  public board: Board;
  public states: StateTask[];

  constructor(private boardService: BoardService, private notification: NotificationService) {
    this.board = new Board([]);
    this.states = [];
    this.currentTask = new TaskKanban(0, '', '', '', new Date());
    this.currentState = new StateTask(0, '');
  }

  ngOnInit(): void {
    this.initBoard();
    this.initStates();
  }

  public changeState(task: TaskKanban, state: StateTask): void {
    this.boardService.changeTaskState(task.id, state.id).subscribe({
      error: err => {
        this.showErrorMessages(err.error.messages);
      },
      complete: () => {
        this.resetCurrentTask();
        task.stateTask = state.stateName;
      }
    });
  }

  public getStates(actualState: string): StateTask[] {
    return this.states.filter(state => state.stateName !== actualState);
  }

  public setCurrentTask(task: TaskKanban): void {
    this.currentTask = task;
  }

  public setCurrentState(state: StateTask): void {
    this.currentState = state;
  }

  public saveTask(): void {
    if (this.currentTask.id === 0) {
      this.createTask();
    } else {
      this.updateTask();
    }
  }

  public getTasksByState(state: StateTask): TaskKanban[] {
    return this.board.tasks.filter(task => task.stateTask === state.stateName);
  }

  private createTask(): void {
    this.boardService.saveTask(new TaskRequest(this.currentTask.taskName, this.currentTask.description,
      this.currentState.id)).subscribe({
      next: response => {
        this.board.addTask(response);
      },
      error: err => {
        this.showErrorMessages(err.error.messages);
      },
      complete: () => {
        this.resetCurrentTask();
        this.notification.okMessage('Tarea agregada con éxito');
      }
    });
  }

  private updateTask(): void {
    this.boardService.updateTask(new TaskRequest(this.currentTask.taskName, this.currentTask.description,
      this.currentState.id), this.currentTask.id).subscribe({
      error: err => {
        this.showErrorMessages(err.error.messages);
      },
      complete: () => {
        this.resetCurrentTask();
        this.notification.okMessage('Tarea actualizada con éxito');
      }
    });
  }

  public deleteTask() {
    this.boardService.deleteTask(this.currentTask.id).subscribe({
      error: err => {
        this.showErrorMessages(err.error.messages);
      },
      complete: () => {
        this.board.removeTask(this.currentTask.id);
        this.resetCurrentTask();
        this.notification.okMessage('Tarea eliminada con éxito');
      }
    });
  }

  public resetCurrentTask() {
    this.currentTask = new TaskKanban(0, '', '', '', new Date());
  }

  public showErrorMessages(errors: string[]): void {
    for (const error of errors) {
      this.notification.errorMessage(error);
    }
  }

  private initBoard(): void {
    this.boardService.findBoard().subscribe({
      next: response => {
        this.board = new Board(response);
      },
      error: err => {
        this.showErrorMessages(err.error.messages);
      }
    });
  }

  private initStates(): void {
    this.boardService.findStates().subscribe({
      next: response => {
        this.states = response;
      },
      error: err => {
        this.showErrorMessages(err.error.messages);
      }
    });
  }

}
