import { Component, OnInit } from '@angular/core';
import {AuthService} from "../../../services/auth.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {

  public username: string;

  constructor(private authService: AuthService, private router: Router) {
    this.username = authService.loggedInUsername;
  }

  ngOnInit(): void {
  }

  public logout(): void {
    this.authService.logOut();
    this.router.navigate(['/sign-in']).then(r => console.log(r));
  }

}
