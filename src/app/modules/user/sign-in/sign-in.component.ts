import { Component, OnInit } from '@angular/core';
import {AuthService} from "../../../services/auth.service";
import {Router} from "@angular/router";
import {NgForm} from "@angular/forms";
import {LoginRequest} from "../../../dtos/request/auth/LoginRequest";
import {NotificationService} from "../../../services/notification.service";

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css']
})
export class SignInComponent implements OnInit {

  public username: string;
  public password: string;

  constructor(private authService: AuthService, private router: Router, private notification: NotificationService) {
    this.username = '';
    this.password = '';
  }

  ngOnInit(): void {
    if (this.authService.isLogin()) {
      this.router.navigateByUrl('').then(() => {});
    }
  }

  public login(form: NgForm) {
    this.authService.login(new LoginRequest(this.username, this.password)).subscribe({
      error: error => {
        this.notification.errorMessage(error.error.messages[0]);
        form.resetForm();
      },
      next: response => {
        this.authService.addTokenToLocalCache(response.headers.get('Jwt-Token'));
      },
      complete: () => {
        this.router.navigate(['/']).then(() => {});
      }
    });
  }

}
