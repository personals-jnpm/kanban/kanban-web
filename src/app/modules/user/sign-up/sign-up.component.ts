import { Component, OnInit } from '@angular/core';
import {RegisterRequest} from "../../../dtos/request/auth/RegisterRequest";
import {AuthService} from "../../../services/auth.service";
import {Router} from "@angular/router";
import {NgForm} from "@angular/forms";
import {NotificationService} from "../../../services/notification.service";

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {

  public user: RegisterRequest;
  public loading: boolean;

  constructor(private authService: AuthService, private router: Router, private notification: NotificationService) {
    this.user = new RegisterRequest('', '', '', '');
    this.loading = false;
  }

  ngOnInit(): void {
    if (this.authService.isLogin()) {
      this.router.navigateByUrl('').then(() => {});
    }
  }

  public register(form: NgForm): void {
    this.loading = true;
    this.authService.register(this.user).subscribe({
      error: err => {
        this.getErrorMessages(err.error.messages);
        this.loading = false;
        form.resetForm();
      },
      complete:() => {
        this.loading = false;
        this.notification.okMessage('Registro exitoso');
        this.notification.okMessage(`Su contraseña ha sido enviada al correo ${this.user.email}`);
        form.resetForm();
        this.router.navigate(['/sign-in']).then(() => {});
      }
    });
  }

  public getErrorMessages(errors: string[]): void {
    for (const error of errors) {
      this.notification.errorMessage(error);
    }
  }

}
