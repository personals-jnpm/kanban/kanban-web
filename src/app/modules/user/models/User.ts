export class User {

  private _userId: string;
  private _firstName: string;
  private _lastName: string;
  private _username: string;
  private _email: string;
  private _profileImageURL: string;

  constructor(userId: string, firstName: string, lastName: string, username: string, email: string, profileImageURL: string) {
    this._userId = userId;
    this._firstName = firstName;
    this._lastName = lastName;
    this._username = username;
    this._email = email;
    this._profileImageURL = profileImageURL;
  }

  get userId(): string {
    return this._userId;
  }

  set userId(value: string) {
    this._userId = value;
  }

  get firstName(): string {
    return this._firstName;
  }

  set firstName(value: string) {
    this._firstName = value;
  }

  get lastName(): string {
    return this._lastName;
  }

  set lastName(value: string) {
    this._lastName = value;
  }

  get username(): string {
    return this._username;
  }

  set username(value: string) {
    this._username = value;
  }

  get email(): string {
    return this._email;
  }

  set email(value: string) {
    this._email = value;
  }

  get profileImageURL(): string {
    return this._profileImageURL;
  }

  set profileImageURL(value: string) {
    this._profileImageURL = value;
  }
}
