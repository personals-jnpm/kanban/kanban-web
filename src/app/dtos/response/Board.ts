import {TaskKanban} from "./TaskKanban";

export class Board {
  public tasks: TaskKanban[];

  constructor(tasks: TaskKanban[]) {
    this.tasks = tasks;
  }

  public removeTask(id: number): void {
    this.tasks = this.tasks.filter(task => task.id !== id);
  }

  public addTask(task: TaskKanban): void {
    this.tasks.push(task);
  }
}
