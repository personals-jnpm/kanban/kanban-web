export class TaskKanban {
  public id: number;
  public taskName: string;
  public description: string;
  public stateTask: string;
  public updateDate: Date;

  constructor(id: number, name: string, description: string, stateTask: string, updateDate: Date) {
    this.id = id;
    this.taskName = name;
    this.description = description;
    this.stateTask = stateTask;
    this.updateDate = updateDate;
  }

  public setStateTask(value: string) {
    this.stateTask = value;
  }
}
