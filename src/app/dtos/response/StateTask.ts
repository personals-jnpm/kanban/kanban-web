export class StateTask {
  public id: number;
  public stateName: string;

  constructor(id: number, stateName: string) {
    this.id = id;
    this.stateName = stateName;
  }

}
