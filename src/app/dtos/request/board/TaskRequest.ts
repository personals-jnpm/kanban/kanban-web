export class TaskRequest {
  public taskName: string;
  public description: string;
  public state: number;

  constructor(taskName: string, description: string, state: number) {
    this.taskName = taskName;
    this.description = description;
    this.state = state;
  }
}
