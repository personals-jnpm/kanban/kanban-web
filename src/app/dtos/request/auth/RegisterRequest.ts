export class RegisterRequest {

  public firstName: string;
  public lastName: string;
  public username: string;
  public email: string;

  constructor(firstName: string, lastName: string, username: string, email: string) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.username = username;
    this.email = email;
  }
}
