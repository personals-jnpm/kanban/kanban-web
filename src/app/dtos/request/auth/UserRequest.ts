export class UserRequest {

  private _currentUsername: string;
  private _username: string;
  private _firstName: string;
  private _lastName: string;
  private _email: string;
  private _role: string;
  private _isNotLocked: boolean;
  private _isActive: boolean;

  constructor(currentUsername: string, username: string, firstName: string, lastName: string, email: string, role: string, isNotLocked: boolean, isActive: boolean) {
    this._currentUsername = currentUsername;
    this._username = username;
    this._firstName = firstName;
    this._lastName = lastName;
    this._email = email;
    this._role = role;
    this._isNotLocked = isNotLocked;
    this._isActive = isActive;
  }

  get currentUsername(): string {
    return this._currentUsername;
  }

  set currentUsername(value: string) {
    this._currentUsername = value;
  }

  get username(): string {
    return this._username;
  }

  set username(value: string) {
    this._username = value;
  }

  get firstName(): string {
    return this._firstName;
  }

  set firstName(value: string) {
    this._firstName = value;
  }

  get lastName(): string {
    return this._lastName;
  }

  set lastName(value: string) {
    this._lastName = value;
  }

  get email(): string {
    return this._email;
  }

  set email(value: string) {
    this._email = value;
  }

  get role(): string {
    return this._role;
  }

  set role(value: string) {
    this._role = value;
  }

  get isNotLocked(): boolean {
    return this._isNotLocked;
  }

  set isNotLocked(value: boolean) {
    this._isNotLocked = value;
  }

  get isActive(): boolean {
    return this._isActive;
  }

  set isActive(value: boolean) {
    this._isActive = value;
  }
}
