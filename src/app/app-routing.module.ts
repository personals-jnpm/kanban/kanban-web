import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {KanbanComponent} from "./modules/board/kanban/kanban.component";
import {SignInComponent} from "./modules/user/sign-in/sign-in.component";
import {SignUpComponent} from "./modules/user/sign-up/sign-up.component";
import {AuthGuard} from "./guards/auth.guard";

const routes: Routes = [
  {path: '', component: KanbanComponent, canActivate: [AuthGuard]},
  {path: 'sign-in', component: SignInComponent},
  {path: 'sign-up', component: SignUpComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
