import { Injectable } from '@angular/core';
import {ToastrService} from "ngx-toastr";

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  constructor(private toasterService: ToastrService) { }

  public errorMessage(message: string): void {
    const parameters = {
      closeButton: true,
      enableHtml: true,
      progressBar: true,
      timeOut: 4000,
      positionClass: 'toast-top-right',
    };
    this.toasterService.warning(
      message,
      'Advertencia',
      parameters
    );
  }

  public okMessage(message: string): void {
    const parameters = {
      closeButton: true,
      enableHtml: true,
      progressBar: true,
      timeOut: 4000,
      positionClass: 'toast-top-right',
    };
    this.toasterService.success(
      message,
      'Operación exitosa',
      parameters
    );
  }
}
