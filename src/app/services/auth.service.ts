import { Injectable } from '@angular/core';
import {environment} from "../../environments/environment";
import {Observable} from "rxjs";
import {HttpClient, HttpResponse} from "@angular/common/http";
import {User} from "../modules/user/models/User";
import {LoginRequest} from "../dtos/request/auth/LoginRequest";
import {RegisterRequest} from "../dtos/request/auth/RegisterRequest";
import {JwtHelperService} from "@auth0/angular-jwt";

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private _userHostUrl: string = environment.apiUserUrl;
  private _token: string;
  private _loggedInUsername: string;
  private jwtHelper: JwtHelperService;

  constructor(private http: HttpClient) {
    this._token = '';
    this._loggedInUsername = '';
    this.jwtHelper = new JwtHelperService();
  }

  public login(request: LoginRequest): Observable<HttpResponse<User>> {
    return this.http.post<User>(this._userHostUrl + 'login', request, {observe: 'response'});
  }

  public register(request: RegisterRequest): Observable<User> {
    return this.http.post<User>(this._userHostUrl + 'register', request);
  }

  get loggedInUsername(): string {
    return this._loggedInUsername;
  }

  get userHostUrl(): string {
    return this._userHostUrl;
  }

  public logOut(): void {
    this._token = '';
    this._loggedInUsername = '';
    localStorage.removeItem('token');
  }

  get token(): string {
    return this._token;
  }

  public addTokenToLocalCache(token: string | null): void {
    if (token != null) {
      this._token = token;
      localStorage.setItem('token', token);
    }
  }

  public loadToken(): void {
    // @ts-ignore
    this._token = localStorage.getItem('token');
  }

  public isLogin(): boolean {
    this.loadToken();
    if (this._token != null && this._token != '') {
      return this.validateToken();
    } else {
      this.logOut();
      return false;
    }
  }

  private validateToken(): boolean {
    try {
      if (this.jwtHelper.decodeToken(this._token).sub != null || '' && !this.jwtHelper.isTokenExpired(this._token)) {
        this._loggedInUsername = this.jwtHelper.decodeToken(this._token).sub;
        return true;
      }
    } catch (e) {
      this.logOut();
    }
    return false;
  }

}
