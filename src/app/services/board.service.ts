import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {TaskKanban} from "../dtos/response/TaskKanban";
import {StateTask} from "../dtos/response/StateTask";
import {environment} from "../../environments/environment";
import {TaskRequest} from "../dtos/request/board/TaskRequest";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class BoardService {

  private _userHostUrl: string = environment.apiBoardUrl;

  constructor(private http: HttpClient) {
  }

  public saveTask(model: TaskRequest): Observable<TaskKanban> {
    return this.http.post<TaskKanban>(this._userHostUrl + 'board', model);
  }

  public updateTask(model: TaskRequest, id: number): Observable<TaskKanban>  {
    return this.http.put<TaskKanban>(this._userHostUrl + `board/${id}`, model);
  }

  public changeTaskState(id: number, state: number): Observable<TaskKanban>  {
    return this.http.put<TaskKanban>(this._userHostUrl + `board/${id}/changeState/${state}`, '');
  }

  public deleteTask(id: number): Observable<Object> {
    return this.http.delete(this._userHostUrl + `board/${id}`);
  }

  public findBoard(): Observable<TaskKanban[]> {
     return this.http.get<TaskKanban[]>(this._userHostUrl + 'board');
/*    return new Board('to do', [
        new TaskKanban(1, 'Add base project', 'We must add the basic project structures to our application', 'to do', new Date()),
        new TaskKanban(2, 'Implement clean code', 'Implement the hexagonal architecture in the project', 'to do', new Date()),
        new TaskKanban(3,'Implement continuous integration', 'Implement Jenkins for continue integration', 'to do', new Date()),
        new TaskKanban(4, 'Dockerize application', 'Dockerize application and deploy it', 'to do', new Date()),
        new TaskKanban(5,'Make business logic', 'Make the uses case of the application', 'in progress', new Date()),
        new TaskKanban(6,'Connect database', 'Make the connection with a database engine', 'in progress', new Date()),
        new TaskKanban(7,'Design the application', 'Design the diagrams and the architecture of the application', 'done', new Date()),
      ]);*/
  }

  public findStates(): Observable<StateTask[]> {
    return this.http.get<StateTask[]>(this._userHostUrl + 'state');
/*    return [
      new StateTask(1, 'to do'),
      new StateTask(2, 'in progress'),
      new StateTask(3, 'done'),
    ];*/
  }
}
